import sys
import argparse
import json
from operator import itemgetter

parser = argparse.ArgumentParser(description='This program aggregates information regarding cpu timeline of a tensorflow graph. To be used with TensorFlow\'s Chrome trace formated json.')

parser.add_argument('filename')
args = parser.parse_args()
raw_source = open(args.filename)
decoded_json = json.load(raw_source)

#operation_list = []
#operation_count = {}

# eliminate 'M' type entries as they don't conform to structure
while decoded_json['traceEvents'][0]['ph'] == "M":
    del decoded_json['traceEvents'][0]
# sort in ascending order of operation start time
#source = sorted(decoded_json['traceEvents'], key=itemgetter('ts'))
source = decoded_json['traceEvents'];

orig_stdout = sys.stdout
f = open('Timeline.txt', 'w')
sys.stdout = f
start_time = 0
finish_time = 0
total = 0

for j in source:
    if j['ph'] == 'X':
        '''
        and (j["name"] == 'Conv2DBackpropFilter' or j["name"] == 'Conv2DBackpropInput' or j["name"] == 'MatMul'  or j["name"] == 'Conv2D' or j["name"] == 'Mul' or j["name"] == 'Slice' or j["name"] == 'Tile' or j["name"] == 'ApplyAdam' or j["name"] == 'Sum' or j["name"] == 'SquaredDiffernece'):
        '''
        if start_time == 0:
	    start_time = j['ts']
        #if j['name'] not in operation_list:
        #    operation_list.append(j['name'])
        #    operation_count[j['name']] = 0
        #operation_count[j['name']] += 1
	#print str(j['name'] +'_' + str(operation_count[j['name']]) + ', ' + str(j['ts'] - start_time)  + ', ' + str(j['dur']) + ', ' +  str(j['ts'] - start_time + j['dur']))
        total = total + 1
	print str(total) + ', ' + str(j['name']) + ', ' + str(j['args']['name']) + ', ' + str(j['ts'] - start_time)  + ', ' + str(j['dur']) + ', ' +  str(j['ts'] - start_time + j['dur'])
        for num in range(0,20):
            i = 'input' + str(num)
            if i in j['args']:
                print ', ' + str(j['args'][i])

	if finish_time < j['ts'] + j['dur']:
	    finish_time = j['ts'] + j['dur']

print str('Overall ' + str((finish_time - start_time) * 0.001) + ' ms')	

sys.stdout = orig_stdout
f.close()


