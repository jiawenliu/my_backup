import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb


path = []
path.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropFilter")
path.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropInput")
path.append("/global/homes/j/jiawen/data/single_op/Conv2D")
path.append("/global/homes/j/jiawen/data/single_op/AddN")
path.append("/global/homes/j/jiawen/data/single_op/ReShape")
path.append("/global/homes/j/jiawen/data/single_op/AvgPool")
path.append("/global/homes/j/jiawen/data/single_op/test")

path_wo = []
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropFilter_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropInput_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropConv_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/AddN_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ReShape_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/AvgPool_wo")

size = []
#size.append(4)
#size.append(5)
#size.append(6)
#size.append(3)
#size.append(3)
#size.append(2)

size.append(3)
size.append(4)
size.append(5)
size.append(2)
size.append(2)
size.append(1)
size.append(6)

column = 34

total_size = 0

start = 6
end = 7

for index in range(start, end):
	total_size = total_size + size[index]

data = [[]for i in range(column*total_size)]

#Load data and train
for index in range(start, end):
	s = [[]for i in range(column*size[index])]
	best_time = []
	best_thread = []

	list_dirs = os.walk(path[index]) 
	for root, dirs, files in list_dirs:     
		dirs.sort()
		dir_index = 0
		#d includes all data in one input size of an operation
		for d in dirs: 
			threads = 2
			my_path = os.path.join(root, d)
			for i in range(column):
				#line_counter = 0
				for file in os.listdir(my_path): 
					f = open(os.path.join(my_path,file))
					iter_f = iter(f)
					counter = 0
					for line in iter_f: 
						if index == 0 or index == 1: 
							value = line[:-1].split("\t")[1]
						else:
							value = line[:-1].split("\t")[0]	
						if counter == i:
							s[i+(dir_index*column)].append(value)	
						counter=counter+1	
					#line_counter = line_counter + 1
					#if line_counter == 15:
					#	break				
				s[i+(dir_index*column)].append(threads)
				threads+=2	
			dir_index+=1
	#Add opt_threads to each item
	for i in range(size[index]):
		best_time.append(float("inf"))
		best_thread.append(2)
		threads = 2
		for j in range(column):
			if threads == 2:
				best_time[i] = s[i * column + j][15]
			elif s[i * column + j][15] < best_time[i]:
				best_time[i] = s[i * column + j][15]
				best_thread[i] = threads
			threads = threads + 2

		for j in range(column):	
			s[i * column + j].append(best_thread[i])

	#Add s to the total data
	current_size = 0;
	for i in range(start, index):
		current_size = current_size + size[i]

	for i in range(len(s)):
		for j in range(len(s[0])):
				data[current_size * column +i].append(s[i][j]) 
	#print best_time
	print best_thread

#Store data
df = pd.DataFrame(data)
df.to_csv("/global/homes/j/jiawen/data/single_op/svc_linear_backup_train.csv", header=None, index=None)

#Load data
#df = pd.read_csv("/global/homes/j/jiawen/data/single_op/svc_linear_backup.csv")
#data = df.values.tolist()

#Generate input and target
input = [[]for i in range(column*total_size)]
target = [0 for i in range (len(data))]

#Copy data from data[][] to input and target
for d1 in range (len(data)):
	for d2 in range(len(data[0])-1):
		input[d1].append(data[d1][d2])

for d1 in range(len(data)):
	target[d1] = data[d1][28]

#Train data
clf = lgb.LGBMClassifier(objective='multiclass',num_leaves=31,learning_rate=0.05,n_estimators=20)
clf.fit(input, target)
#Store model
joblib.dump(clf, "/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

#Load model
clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")
#Predict results
print clf.predict([input[column*15+33]])

#clf = svm.SVC(kernel = 'linear')
#clf.fit(input[:-1], target[:-1])

#Model predict
#print clf.decision_function([data[34*1+0]]) 

#size.append(3) 0-2
#size.append(4) 3-6
#size.append(5) 8-12
#size.append(2) 13-14
#size.append(2) 15-16
#size.append(1) 17


#size.append(4) 0-3
#size.append(5) 4-8
#size.append(6) 9-14
#size.append(3) 15-17
#size.append(3) 18-20
#size.append(2) 21-22

#[18, 14, 16, 16]
#[18, 16, 18, 18, 16]
#[16, 16, 20, 16, 16, 20]
#[44, 60, 8]
#[2, 2, 2]
#[46, 58]
#clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")
#print clf.predict([input[column*0+33]]) 
