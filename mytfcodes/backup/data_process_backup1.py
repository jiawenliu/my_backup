import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib

path = []
path.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropFilter")
path.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropInput")
path.append("/global/homes/j/jiawen/data/single_op/Conv2D")
path.append("/global/homes/j/jiawen/data/single_op/AddN")
path.append("/global/homes/j/jiawen/data/single_op/ReShape")
path.append("/global/homes/j/jiawen/data/single_op/AvgPool")

path_wo = []
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropFilter_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropInput_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropConv_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/AddN_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ReShape_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/AvgPool_wo")

size = []
size.append(4)
size.append(5)
size.append(6)
size.append(3)
size.append(3)
size.append(2)

column = 34

total_size = 0

for index in range(0, 6):
	total_size = total_size + size[index]

data = [[]for i in range(column*total_size)]

for index in range(0, 6):
	s = [[]for i in range(column*size[index])]
	best_time = []
	best_thread = []

	list_dirs = os.walk(path[index]) 
	for root, dirs, files in list_dirs:     
		dirs.sort()
		dir_index = 0
		#d includes all data in one input size of an operation
		for d in dirs: 
			threads = 2
			my_path = os.path.join(root, d)
			for i in range(column):
				for file in os.listdir(my_path): 
					f = open(os.path.join(my_path,file))
					iter_f = iter(f)
					counter = 0
					for line in iter_f: 
						if index == 0 or index == 1: 
							value = line[:-1].split("\t")[1]
						else:
							value = line[:-1].split("\t")[0]	
						if counter == i:
							s[i+(dir_index*column)].append(value)	
						counter=counter+1				
				s[i+(dir_index*column)].append(threads)
				threads+=2	
			dir_index+=1
	#Add opt_threads to each item
	for i in range(size[index]):
		best_time.append(float("inf"))
		best_thread.append(2)
		threads = 2
		for j in range(column):
			if threads == 2:
				best_time[i] = s[i * column + j][15]
			elif s[i * column + j][15] < best_time[i]:
				best_time[i] = s[i * column + j][15]
				best_thread[i] = threads
			threads = threads + 2

		for j in range(column):	
			s[i * column + j].append(best_thread[i])

	#Add s to the total data
	current_size = 0;
	for i in range(0, index):
		current_size = current_size + size[i]

	for i in range(len(s)):
		for j in range(len(s[0])):
				data[current_size * 34 +i].append(s[i][j]) 
	#print best_time
	print best_thread

#Train the model
input = [[]for i in range(column*total_size)]
target = [0 for i in range (len(data))]

for d1 in range (len(data)):
	for d2 in range(len(data[0])-1):
		input[d1].append(data[d1][d2])

for d1 in range(len(data)):
	target[d1] = data[d1][28]

#print input
#print target

clf = svm.SVC(kernel="linear")

clf.fit(input[:-1], target[:-1])

joblib.dump(clf, "/global/homes/j/jiawen/mytfcodes/svc_linear.pkl")
print "Saved model"
#Model predict
#print clf.decision_function([data[34*1+0]]) 

#size.append(4) 0-3
#size.append(5) 4-8
#size.append(6) 9-14
#size.append(3) 15-17
#size.append(3) 18-20
#size.append(2) 21-22

#[18, 14, 16, 16]
#[18, 16, 18, 18, 16]
#[16, 16, 20, 16, 16, 20]
#[44, 60, 8]
#[2, 2, 2]
#[46, 58]
clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear.pkl")
print clf.predict([input[34*0+0]]) 

#df = pd.DataFrame(data)
#df.to_csv("/global/homes/j/jiawen/data/single_op/test.csv", header=None, index=None)
#df.to_csv("/global/homes/j/jiawen/data/single_op/total.csv", mode='a', header=None, index=None)






