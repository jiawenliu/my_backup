import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb


path = []
path.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropFilter")
path.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropInput")
path.append("/global/homes/j/jiawen/data/single_op/Conv2D")
path.append("/global/homes/j/jiawen/data/single_op/AddN")
path.append("/global/homes/j/jiawen/data/single_op/ReShape")
path.append("/global/homes/j/jiawen/data/single_op/AvgPool")
path.append("/global/homes/j/jiawen/data/single_op/test")

path_wo = []
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropFilter_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropInput_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/Conv2DBackpropConv_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/AddN_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ReShape_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/AvgPool_wo")

size = []
#size.append(4)
#size.append(5)
#size.append(6)
#size.append(3)
#size.append(3)
#size.append(2)

size.append(3)
size.append(4)
size.append(5)
size.append(2)
size.append(2)
size.append(1)
size.append(6)

column = 34

total_size = 0

start = 6
end = 7

for index in range(start, end):
	total_size = total_size + size[index]

data = [[]for i in range(column*total_size)]

#For prediction testing
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/svc_linear_backup_test.csv")
data = df.values.tolist()

input = [[]for i in range(column*total_size)]
target = [0 for i in range (len(data))]

for d1 in range (len(data)):
	for d2 in range(len(data[0])-1):
		input[d1].append(data[d1][d2])

for d1 in range(len(data)):
	target[d1] = data[d1][28]

#print input
#print target	
tmp = [[]for i in range(34)]

clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")
print clf.decision_function([input[column*5+32]])
print clf.predict([input[column*5+32]])
'''
for j in range(33):
	for i in range(size[start]):
		tmp[j].append(clf.predict([input[column*i+j]]))
for j in range(33):		
	print tmp[j]	
'''
#[18, 14, 16, 16] {0-3}
#[18, 16, 18, 18, 16] {4-8}
#[16, 16, 20, 16, 16, 20] {9-14}
#[44, 60, 8] {15-17}
#[2, 2, 2] {18-20}
#[46, 58] {21-22}

#[68, 2, 20,  16, 58, 8]
#predict
#[2, 2, 16,  2, 20, 16]

