#!/bin/bash
#size=(8192 1024 2048 1024 8192 8192 8192 256)

file=mul.py
result=/home/jours/intel/mulresult
report=/home/jours/intel/mulreport

input0=(20 50 100 200 300 400 500)
input1=(0 100 200 300 500 700 1000)
input2=(0 100 200 300 500 700 1000)
input3=(0 3 5 10 20)

rm -rf $result
rm -rf $report

mkdir $result
mkdir $report

defin=(${input0[0]} ${input1[0]} ${input2[0]} ${input3[0]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[0]} ${input3[0]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[0]} ${input3[0]})
for input in ${input1[@]}
do
	defin[1]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[2]} ${input3[0]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[2]} ${input3[0]})
for input in ${input1[@]}
do
	defin[1]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[2]} ${input3[0]})
for input in ${input2[@]}
do
	defin[2]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[2]} ${input3[2]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[2]} ${input3[2]})
for input in ${input1[@]}
do
	defin[1]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[2]} ${input3[2]})
for input in ${input2[@]}
do
	defin[2]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[3]} ${input1[2]} ${input2[2]} ${input3[2]})
for input in ${input3[@]}
do
	defin[3]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done



