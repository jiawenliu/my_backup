import os
import argparse
import tensorflow as tf
import time as time

from tensorflow.python.client import timeline

parser = argparse.ArgumentParser(description='This program tests conv2d_backprop_filter performance and dumps 1000 runs.')
#parser.add_argument('num_cpu', type=int)
parser.add_argument('size', type=int)
parser.add_argument('intra', type=int)
args = parser.parse_args()

dimsize = args.size
num_intra = args.intra
num_cpu = 32
dev = '/cpu:0'
#if args.device == 1:
#  dev = '/gpu:0'

with tf.device(dev):
	input = tf.Variable(tf.random_normal([1,dimsize,dimsize,3]))  # 1 image, 3x3, 3 channels
	filter = tf.Variable(tf.random_normal([3,3,3,3])) # 3x3, 3 channels in, 3 channels out 
	layer1 = tf.nn.conv2d(input, filter, strides=[1, 1, 1, 1], padding='VALID')


if __name__ == '__main__':
    #dumpdir = 'conv2d_dump_i%dx%d_f%xx%d' % (dimsize, dimsize, args.filterw, args.filterh)
    #os.mkdir(dumpdir)
	config = tf.ConfigProto(device_count = {"CPU": num_cpu})
	#config.gpu_options.allow_growth = True
	config.intra_op_parallelism_threads = num_intra
	config.inter_op_parallelism_threads  = 1

	with tf.Session(config=config) as sess:
		sess.run(tf.global_variables_initializer())
		
		bench=tf.test.Benchmark()
		print tf.test.Benchmark.run_op_benchmark(bench,sess,layer1,store_memory_usage=True,min_iters=100)
