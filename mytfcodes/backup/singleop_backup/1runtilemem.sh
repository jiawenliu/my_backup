#!/bin/bash
#size=(8192 1024 2048 1024 8192 8192 8192 256)

file=tile.py
result=/home/jours/intel/tileresult
report=/home/jours/intel/tilereport

input0=(20 30 40)
input1=(5 10 20 30 60 90 120 200)
input2=(30 60 90 120 200 400 600)

rm -rf $result
rm -rf $report

mkdir $result
mkdir $report

defin=(${input0[1]} ${input1[3]} ${input2[3]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[3]} ${input2[3]})
for input in ${input1[@]}
do
	defin[1]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[3]} ${input2[3]})
for input in ${input2[@]}
do
	defin[2]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${num}
		
		((num=num*2));
	done
done


