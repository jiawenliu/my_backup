import os
import argparse
import tensorflow as tf
import time as time

from tensorflow.python.client import timeline

'''

'''

parser = argparse.ArgumentParser()
#parser.add_argument('num_cpu', type=int)
parser.add_argument('size1', type=int)
parser.add_argument('size2', type=int)
parser.add_argument('size3', type=int)
parser.add_argument('intra', type=int)
args = parser.parse_args()

dimsize1 = args.size1
dimsize2 = args.size2
dimsize3 = args.size3
num_intra = args.intra
num_cpu = 32
num_inter = 1
dev = '/cpu:0'
#if args.device == 1:
#  dev = '/gpu:0'


with tf.device(dev):
	reduce_mean = tf.Variable(tf.random_normal([dimsize1,dimsize2]))  # 1 image, 3x3, 3 channels
	layer1 = tf.reduce_mean(inputX,inputY)


if __name__ == '__main__':
    #dumpdir = 'conv2d_dump_i%dx%d_f%xx%d' % (dimsize, dimsize, args.filterw, args.filterh)
    #os.mkdir(dumpdir)
	config = tf.ConfigProto(device_count = {"CPU": num_cpu})
	#config.gpu_options.allow_growth = True
	config.intra_op_parallelism_threads = num_intra
	config.inter_op_parallelism_threads  = num_inter

	with tf.Session(config=config) as sess:
		sess.run(tf.global_variables_initializer())
		
		bench=tf.test.Benchmark()
		time=0.0
		iters=1
		for i in range(iters):
			result=tf.test.Benchmark.run_op_benchmark(bench,sess,layer1,store_memory_usage=True,min_iters=1000)
			#time=time+float(result["wall_time"])
		#print 'time of reduce_mean: %f'%(time/iters)