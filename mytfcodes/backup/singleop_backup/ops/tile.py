import os
import argparse
import tensorflow as tf
import time as time

from tensorflow.python.client import timeline

'''
This operation creates a new tensor by replicating input multiples times. The output tensor's i'th dimension has input.dims(i) * multiples[i] elements, and the values of input are replicated multiples[i] times along the 'i'th dimension. For example, tiling [a b c d] by [2] produces [a b c d a b c d].
'''

parser = argparse.ArgumentParser()
parser.add_argument('size0', type=int)
parser.add_argument('size1', type=int)
parser.add_argument('size2', type=int)
parser.add_argument('intra', type=int)
args = parser.parse_args()

dimsize0 = args.size0
dimsize1 = args.size1
dimsize2 = args.size2
num_intra = args.intra
num_cpu = 32
num_inter = 1
dev = '/cpu:0'
#if args.device == 1:
#  dev = '/gpu:0'

with tf.device(dev):
	input = tf.Variable(tf.random_normal([1,dimsize,dimsize,3]))  # 1 image, 3x3, 3 channels
	inputY = [2,2,2,2]  # 1 image, 3x3, 3 channels
	layer1 = tf.tile(input,inputY)


if __name__ == '__main__':
    #dumpdir = 'conv2d_dump_i%dx%d_f%xx%d' % (dimsize, dimsize, args.filterw, args.filterh)
    #os.mkdir(dumpdir)
	config = tf.ConfigProto(device_count = {"CPU": num_cpu})
	#config.gpu_options.allow_growth = True
	config.intra_op_parallelism_threads = num_intra
	config.inter_op_parallelism_threads  = num_inter

	with tf.Session(config=config) as sess:
		sess.run(tf.global_variables_initializer())
		
		bench=tf.test.Benchmark()
		time=0.0
		iters=10
		for i in range(iters):
			result=tf.test.Benchmark.run_op_benchmark(bench,sess,layer1,store_memory_usage=True,min_iters=100)
			time=time+float(result["wall_time"])
		print 'time of tile: %f'%(time/iters)