import argparse
import json
from operator import itemgetter

parser = argparse.ArgumentParser(description='This program takes a folder of 50 json files and an operation type and calculates the average run time of that operation.')
parser.add_argument('directory')
parser.add_argument('operation')
args = parser.parse_args()

# sort in ascending order of operation start time
sum_of_op = 0
count_of_op = 0
op_thread = -1
coarse_op_time = 0
fine_op_time = 0

for i in range(50):
  raw_source = open('%s/timeline_step_%d.json' % (args.directory, i))
  decoded_json = json.load(raw_source)['traceEvents']
  # eliminate 'M' type entries as they don't conform to structure
  while decoded_json[0]['ph'] == "M":
    del decoded_json[0]
  for x in decoded_json:
    if x['name'] == args.operation and x['ph'] == 'X':
      if 'input0' in x['args']:
        coarse_op_time = x['dur']
      else:
        if op_thread != x['pid']:
          op_thread = x['pid']
        fine_op_time += x['dur']
  count_of_op += 1
  if coarse_op_time > fine_op_time:
    sum_of_op += coarse_op_time
  else:
    sum_of_op += fine_op_time
  print(sum_of_op)
  coarse_op_time = 0
  fine_op_time = 0
  op_thread = -1

print 'Average ' + args.operation + ' execution time: ' + str(sum_of_op*0.001) + 'ms spent performing ' + str(count_of_op) + ' ' + args.operation + ' operations. ' + str((sum_of_op/count_of_op)*0.001) + 'ms average operation time.'
