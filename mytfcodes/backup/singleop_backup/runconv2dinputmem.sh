#!/bin/bash
#size=(8192 1024 2048 1024 8192 8192 8192 256)

file=conv2d.py
result=/home/jours/intel/conv2dresult
report=/home/jours/intel/conv2dreport

#rm -rf ${result}
#rm -rf ${report}

#mkdir ${result}
#mkdir ${report}

input0=(20 30 40)
input1=(10 20 30 40 60 80 120 140 200 300)
input2=(30 60 90 120 200 400 600)
input3=(2 4 6 8 10)
input4=(2 4 6 8 10)
input5=(20 50 100 200 300 500 700 1000 1500)

# defin=(${input0[1]} ${input1[5]} ${input2[4]} ${input3[2]} ${input4[2]} ${input5[3]})
# for input in ${input0[@]}
# do
	# defin[0]=$input
	# echo ${defin[@]}
	
	# num=1
	# for ((i=0; i<6; ++i))
	# do
		# echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		# amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num

		
		# echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		# amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		

		# ((num=num*2));
	# done
# done
# defin=(${input0[1]} ${input1[5]} ${input2[4]} ${input3[2]} ${input4[2]} ${input5[3]})
# for input in ${input1[@]}
# do
	# defin[1]=${input1[9]}
	# echo ${defin[@]}
	
	# num=1
	# for ((i=0; i<6; ++i))
	# do
		# echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		# amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num

		
		# echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		# amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		

		# ((num=num*2));
	# done
# done
defin=(${input0[1]} ${input1[5]} ${input2[4]} ${input3[2]} ${input4[2]} ${input5[3]})
for input in ${input2[@]}
do
	defin[2]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num

		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		

		((num=num*2));
	done
done
defin=(${input0[1]} ${input1[5]} ${input2[4]} ${input3[2]} ${input4[2]} ${input5[3]})
for input in ${input3[@]}
do
	defin[3]=$input
	defin[4]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num

		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		

		((num=num*2));
	done
done
defin=(${input0[1]} ${input1[5]} ${input2[4]} ${input3[2]} ${input4[2]} ${input5[3]})
for input in ${input5[@]}
do
	defin[5]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num

		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		

		((num=num*2));
	done
done
