import argparse
import json
import csv
from operator import itemgetter

parser = argparse.ArgumentParser(description='This program takes a folder of 50 json files and an operation type and calculates the average run time of that operation.')
parser.add_argument('maxpow', type=int)
parser.add_argument('operation')
args = parser.parse_args()

# sort in ascending order of operation start time
sum_of_op = 0
count_of_op = 0
op_thread = -1
coarse_op_time = 0
fine_op_time = 0

device = 'cpu'
line = [0]*(args.maxpow)
cpowx = 32
cpowy = 32
k = 0
j = 0
with open("%s_result.csv" % device, 'wb') as file:
    out = csv.writer(file)
    for p in range(args.maxpow):
      for q in range(args.maxpow):
        sum_of_op = 0
        count_of_op = 0
        op_thread = -1
        coarse_op_time = 0
        fine_op_time = 0
        for i in range(50):
          if not (p == args.maxpow-1 and q == args.maxpow-1):
            raw_source = open('%s/conv2d_dump_i%dx%d_f3x3/timeline_step_%d.json' % (device, cpowx, cpowy, i))
            decoded_json = json.load(raw_source)['traceEvents']
            # eliminate 'M' type entries as they don't conform to structure
            while decoded_json[0]['ph'] == "M":
              del decoded_json[0]
            for x in decoded_json:
              if x['name'] == args.operation and x['ph'] == 'X':
                if 'input0' in x['args']:
                  coarse_op_time = x['dur']
                else:
                  if op_thread != x['pid']:
                    op_thread = x['pid']
                  fine_op_time += x['dur']
            count_of_op += 1
            if coarse_op_time > fine_op_time:
              sum_of_op += coarse_op_time
            else:
              sum_of_op += fine_op_time
            coarse_op_time = 0
            fine_op_time = 0
        cpowy *= 2
        op_thread = -1
        if not (p == args.maxpow-1 and q == args.maxpow-1):
          line[k] = (sum_of_op/count_of_op)*0.001
        else:
          line[k] = -1
        k += 1
      cpowy = 32
      cpowx *= 2
      k = 0
      out.writerow(line)
