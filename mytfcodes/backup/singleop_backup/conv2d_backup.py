import os
import argparse
import tensorflow as tf
import time as time
#from pypapi import papi_high
#from pypapi import events as papi_events

from tensorflow.python.client import timeline

parser = argparse.ArgumentParser(description='Profile')

parser.add_argument('filter1', type=int)
parser.add_argument('filter2', type=int)
parser.add_argument('filter3', type=int)
parser.add_argument('filter4', type=int)
args = parser.parse_args()

dimsize = args.size
dev = '/cpu:0'

with tf.device(dev):
	input = tf.Variable(tf.random_normal([64,32,32,3]))  # 1 image, 3x3, 3 channels
	filter = tf.Variable(tf.random_normal([3,3,3,16])) # 3x3, 3 channels in, 3 channels out 
	layer1 = tf.nn.conv2d(input, filter, strides=[1, 1, 1, 1], padding='VALID')


if __name__ == '__main__':
	config = tf.ConfigProto()
	config.intra_op_parallelism_threads = 68
	config.inter_op_parallelism_threads  = 1

	with tf.Session(config=config) as sess:

#		papi_high.start_counters([
#                papi_events.PAPI_TOT_INS,
#                papi_events.PAPI_TOT_CYC
#                ])

		sess.run(tf.global_variables_initializer())
		
		bench=tf.test.Benchmark()
		time =  tf.test.Benchmark.run_op_benchmark(bench,sess,layer1,store_memory_usage=True,min_iters=2)
		print "%.6f" % time["wall_time"]
'''
		results = papi_high.stop_counters()
                INS = results[0]
                CYC = results[1]
                IPC = INS/float(CYC)

		print "Total Instrcutions: %d\n Total Cycles: %d" % (INS, CYC)
                print "IPC: %.2f" % (IPC)
'''
