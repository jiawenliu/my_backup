#!/bin/bash
#size=(8192 1024 2048 1024 8192 8192 8192 256)

file=addn.py
result=/home/jours/intel/addnresult
report=/home/jours/intel/addnreport

input0=(500 1000 1500 2000 2500)
input1=(500 1000 1500 2000 2500)

rm -rf $result
rm -rf $report

mkdir $result
mkdir $report

mkdir /home/jours/intel/addnresult
mkdir /home/jours/intel/addnreport

defin=(${input0[1]} ${input1[3]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${num} python ops/$file ${defin[0]} ${defin[1]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${num} python ops/$file ${defin[0]} ${defin[1]} $num

		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${num} > ${report}/${defin[0]}_${defin[1]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${num} > ${report}/${defin[0]}_${defin[1]}_${num}

		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[3]})
for input in ${input1[@]}
do
	defin[1]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${num} python ops/$file ${defin[0]} ${defin[1]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${num} python ops/$file ${defin[0]} ${defin[1]} $num

		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${num} > ${report}/${defin[0]}_${defin[1]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${num} > ${report}/${defin[0]}_${defin[1]}_${num}

		((num=num*2));
	done
done



