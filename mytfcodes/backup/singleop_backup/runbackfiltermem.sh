#!/bin/bash
#size=(8192 1024 2048 1024 8192 8192 8192 256)

file=conv2DBackpropFilter.py
result=/home/jours/intel/filterresult
report=/home/jours/intel/filterreport

input0=(20 30 40)
input1=(10 20 30 40 60 80 120 140 200 300)
input2=(30 60 90 120 200 400 600)
input3=(20 60 120 300 600 1000 1500 2100)
input4=(2 4 6 8 10)
input5=(2 4 6 8 10)

rm -rf $result
rm -rf $report

mkdir $result
mkdir $report


defin=(${input0[1]} ${input1[5]} ${input2[3]} ${input3[3]} ${input4[2]} ${input5[2]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num

		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[5]} ${input2[3]} ${input3[3]} ${input4[2]} ${input5[2]})
for input in ${input1[@]}
do
	defin[1]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[5]} ${input2[3]} ${input3[3]} ${input4[2]} ${input5[2]})
for input in ${input2[@]}
do
	defin[2]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[5]} ${input2[3]} ${input3[3]} ${input4[2]} ${input5[2]})
for input in ${input3[@]}
do
	defin[3]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[5]} ${input2[3]} ${input3[3]} ${input4[2]} ${input5[2]})
for input in ${input4[@]}
do
	defin[4]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		((num=num*2));
	done
done

defin=(${input0[1]} ${input1[5]} ${input2[3]} ${input3[3]} ${input4[2]} ${input5[2]})
for input in ${input5[@]}
do
	defin[5]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} ${defin[4]} ${defin[5]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${defin[4]}_${defin[5]}_${num}

		((num=num*2));
	done
done


