#!/bin/bash
#size=(8192 1024 2048 1024 8192 8192 8192 256)

file=slice.py
result=/home/jours/intel/sliceresult
report=/home/jours/intel/slicereport

input0=(100 200 300 500 700 1000)
input11=(100 200 300 500 700 1000)
input12=(8 16 32 64)
input2=(1 3 5)
input3=(0 200 400 800 1600)

rm -rf $result
rm -rf $report

mkdir $result
mkdir $report

defin=(${input0[2]} ${input11[2]} ${input2[1]} ${input3[0]})
for input in ${input0[@]}
do
	defin[0]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[2]} ${input11[2]} ${input2[1]} ${input3[0]})
for input in ${input11[@]}
do
	defin[1]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(${input0[2]} ${input11[2]} ${input2[1]} ${input3[0]})
for input in ${input2[@]}
do
	defin[2]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(32 ${input12[1]} ${input12[1]} ${input3[3]})
for input in ${input12[@]}
do
	defin[1]=$input
	defin[2]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

defin=(32 ${input12[1]} ${input12[1]} ${input3[3]})
for input in ${input3[@]}
do
	defin[3]=$input
	echo ${defin[@]}
	
	num=1
	for ((i=0; i<6; ++i))
	do
		rm -rf ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		rm -rf ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}

		echo "amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num"
		amplxe-cl -collect memory-access -knob analyze-openmp=true -knob dram-bandwidth-limits=false -data-limit=0 -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} python ops/$file ${defin[0]} ${defin[1]} ${defin[2]} ${defin[3]} $num
		
		echo "amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}"
		amplxe-cl -report summary -result-dir ${result}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num} > ${report}/${defin[0]}_${defin[1]}_${defin[2]}_${defin[3]}_${num}
		
		((num=num*2));
	done
done

