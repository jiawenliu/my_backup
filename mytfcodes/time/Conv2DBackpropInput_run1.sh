output1=64
output2=8
output3=8
output4=64

filter1=1
filter2=1
filter3=64
filter4=64

PAPI[0]=PAPI_L1_DCM 
PAPI[1]=PAPI_L1_ICM
PAPI[2]=PAPI_L1_TCM
PAPI[3]=PAPI_L2_TCM
PAPI[4]=PAPI_TLB_DM
PAPI[5]=PAPI_L1_LDM
PAPI[6]=PAPI_L2_LDM
PAPI[7]=PAPI_STL_ICY
PAPI[8]=PAPI_BR_UCN
PAPI[9]=PAPI_BR_CN
PAPI[10]=PAPI_BR_TKN
PAPI[11]=PAPI_BR_NTK
PAPI[12]=PAPI_BR_MSP
PAPI[13]=PAPI_TOT_INS
PAPI[14]=PAPI_LD_INS
PAPI[15]=PAPI_SR_INS
PAPI[16]=PAPI_BR_INS
PAPI[17]=PAPI_RES_STL
PAPI[18]=PAPI_TOT_CYC
PAPI[19]=PAPI_LST_INS
PAPI[20]=PAPI_L1_DCA
PAPI[21]=PAPI_L1_ICH
PAPI[22]=PAPI_L1_ICA
PAPI[23]=PAPI_L2_TCH
PAPI[24]=PAPI_L2_TCA
PAPI[25]=PAPI_REF_CYC


export MY_ITER=10
export OP_NUM=1
mkdir /global/homes/j/jiawen/data/single_op_test/Conv2DBackpropInput/$filter1-$filter2-$filter3-$filter4-$output1-$output2-$output3-$output4

#for i in {0..25}
#  do
	export MYPAPI=${PAPI[$i]}
	export MY_PATH=/global/homes/j/jiawen/data/single_op_test/Conv2DBackpropInput/$filter1-$filter2-$filter3-$filter4-$output1-$output2-$output3-$output4/Time
	#for j in {2..68..2}
	for j in {2,34,68}
		do
			export OMP_NUM_THREADS=$j
		        python /global/homes/j/jiawen/mytfcodes/Conv2DBackpropInput.py $filter1 $filter2 $filter3 $filter4 $output1 $output2 $output3 $output4	
	done     
#done
