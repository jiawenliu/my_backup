import os
import argparse
import tensorflow as tf
import numpy

parser = argparse.ArgumentParser(description='Profile')

parser.add_argument('input1', type=int)
parser.add_argument('input2', type=int)
parser.add_argument('input3', type=int)
parser.add_argument('input4', type=int)
parser.add_argument('shape', type=int)

args = parser.parse_args()

dev = '/cpu:0'

with tf.device(dev):

	layer1 = tf.reshape([args.input1,args.input2,args.input3,args.input4], [args.shape])

if __name__ == '__main__':
	config = tf.ConfigProto()
	config.intra_op_parallelism_threads = 68
	config.inter_op_parallelism_threads  = 1

	with tf.Session(config=config) as sess:
		sess.run(tf.global_variables_initializer())
		bench=tf.test.Benchmark()
		tf.test.Benchmark.run_op_benchmark(bench,sess,layer1,store_memory_usage=True,min_iters=int(os.environ['MY_ITER']))
