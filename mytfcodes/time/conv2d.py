import os
import argparse
import tensorflow as tf

parser = argparse.ArgumentParser(description='Profile')

parser.add_argument('input1', type=int)
parser.add_argument('input2', type=int)
parser.add_argument('input3', type=int)
parser.add_argument('input4', type=int)

parser.add_argument('filter1', type=int)
parser.add_argument('filter2', type=int)
parser.add_argument('filter3', type=int)
parser.add_argument('filter4', type=int)
args = parser.parse_args()

dev = '/cpu:0'

with tf.device(dev):
	input = tf.Variable(tf.random_normal([args.input1,args.input2,args.input3,args.input4]))  # 1 image, 3x3, 3 channels
	filter = tf.Variable(tf.random_normal([args.filter1,args.filter2,args.filter3,args.filter4])) # 3x3, 3 channels in, 3 channels out 
	layer1 = tf.nn.conv2d(input, filter, strides=[1, 1, 1, 1], padding='VALID')

if __name__ == '__main__':
	config = tf.ConfigProto()
	config.intra_op_parallelism_threads = 68
	config.inter_op_parallelism_threads  = 1

	with tf.Session(config=config) as sess:
		sess.run(tf.global_variables_initializer())
		bench=tf.test.Benchmark()
		tf.test.Benchmark.run_op_benchmark(bench,sess,layer1,store_memory_usage=True,min_iters=int(os.environ['MY_ITER']))
