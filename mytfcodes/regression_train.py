import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_classification
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
from sklearn.neural_network import MLPRegressor
import linecache
from sklearn import tree, svm, naive_bayes,neighbors
from sklearn.ensemble import BaggingClassifier, BaggingRegressor, AdaBoostClassifier, AdaBoostRegressor, RandomForestClassifier, GradientBoostingRegressor, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression 
from sklearn.grid_search import GridSearchCV
from sklearn.tree import DecisionTreeRegressor


path = "/global/homes/j/jiawen/data/single_op/Inception_68"
time_path = "/global/homes/j/jiawen/data/single_op/Inception_68/Time/Time"

op = []

#Conv2DBackpropFilter
op.append(43)
#Conv2DBackpropInput
op.append(39)
#Conv2D
op.append(48)
#MklInputConversion
op.append(138)
#Mkl2Tf
op.append(123)
#AddN
op.append(85)
#Reshape
op.append(16)
#AvgPool
op.append(20)

total_op = 0
start = 0
end = 8

for i in range(start, end):
	total_op = total_op + op[i]

#column_convert: 0,2...66
#column_mkl2tf:  1,3...67

s = [[]for i in range(total_op)]
my_time = [[]for i in range(total_op)]

#Add op hw data to s
for i in range(total_op):
	for files in os.listdir(path): 
		line = linecache.getline(os.path.join(path,files), i+1)
		if len(line)!=0 and len(line)!=1:
			s[i].append(line[:-1])

#Get each best thread
best_time = []
best_thread = []
total_start = 0
total_end = 0

for i in range (start, end):
	if i == 0:
		total_start = 0
	else:
		total_start= total_start + op[i-1]
	counter = 0	
	for j in range (total_start, total_start + op[i]):
		best_time.append(float("inf"))
		best_thread.append(2)
		for k in range (2,70,2):
			my_path = time_path + str(k)
			line = linecache.getline(my_path, j+1)
			if len(line)!=0 and len(line)!=1:
				value = line[:-1]
				my_time[j].append(value)
				if k == 2 or best_time[j] > value:
					best_time[j] = value
					best_thread[j] = k
				#Add time data to s	
				if k == 68:
					s[j].append(value)	
					s[j].append(i)
					s[j].append(counter)
					counter+=1
		s[j].append(best_thread[j])			

input1 = [[]for i in range((len(s)-7))]
target1 = [[]for i in range((len(s)-7))]

counter = 0
for d1 in range (len(s)):
	if len(s[d1]) > 3:	
		for d2 in range(len(s[d1])):
			if d2 == 26:
				input1[counter].append(float(s[d1][d2]))	
			elif d2 == 3:
				input1[counter].append(float(s[d1][23])/float(s[d1][3]))	
			elif d2 == 9:
				input1[counter].append(float(s[d1][5])/float(s[d1][9]))	
		counter+=1	

print input1

counter = 0
for d1 in range (len(my_time)):
	if len(my_time[d1]) > 3:	
		for d2 in range(len(my_time[d1])):
			if d2 == 33:
				target1[counter].append(float(my_time[d1][d2]))	
		counter+=1	

input = [[]for i in range((len(input1)))]
target = [[]for i in range((len(target1)))]


counter = 0
for d1 in range(len(input1)):
	#if d1 % 30 == 0:
		for d2 in range(len(input1[0])):
			input[counter].append(float(input1[d1][d2]))
		counter+=1

counter = 0
for d1 in range(len(target1)):
		for d2 in range(len(target1[0])):
			target[counter].append(float(target1[d1][d2]))
		counter+=1

#Store data
#df = pd.DataFrame(data)
#df.to_csv("/global/homes/j/jiawen/data/single_op/Inception_data.csv", header=None, index=None)

'''
#Copy data from data[][] to input and target
input = [[]for i in range(len(data))]
target = []

#print s

for d1 in range (len(data)):
		for d2 in range(len(data[0])-1):
			if d2 ==5 or d2 == 9 or d2 ==26:
				input[counter].append(float(data[d1][d2]))		

for d1 in range(len(s)):
		target.append(data[d1][len(data[0])-1])
'''


'''
PAPI_TOT_CYC 3
PAPI_L2_TCM 5
PAPI_L2_TCA 9
PAPI_L1_ICH 15
PAPI_TOT_INS 23
Time 26
Op_type 27
Op_input_index 28
'''
'''
#----SVM --70%
#kernels: linear, sigmoid, rbf, poly
#clf = svm.SVR(kernel = 'linear')
#----decision_tree --90%
#clf = tree.DecisionTreeClassifier()
clf = DecisionTreeRegressor()
#----naive_mul --30%
#clf = naive_bayes.MultinomialNB()
#----naive_gaussian --40%
#clf = naive_bayes.GaussianNB()
#----K_neighbor --70%
#clf = neighbors.KNeighborsClassifier()
#clf = neighbors.KNeighborsRegressor()
#----bagging_knn --50%
#clf = BaggingClassifier(neighbors.KNeighborsClassifier(), max_samples=0.5,max_features=0.5)
#clf = BaggingRegressor(neighbors.KNeighborsClassifier(), max_samples=0.5,max_features=0.5)
#----bagging_tree --85%
#clf = BaggingClassifier(tree.DecisionTreeClassifier(), max_samples=0.5,max_features=0.5)
#clf = BaggingClassifier(tree.DecisionTreeRegressor(), max_samples=0.5,max_features=0.5)
#----random_forest --90%
#clf = RandomForestClassifier(n_estimators=50)
#clf = RandomForestRegressor(n_estimators=50)
#----adaboost --60%
#clf = AdaBoostClassifier(n_estimators=50)
#clf = AdaBoostRegressor(n_estimators=50)
#----Logistic Regression --10%
#clf = LogisticRegression(penalty='l2')
#----gradient_boost --10%
#clf = GradientBoostingClassifier(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0)
#clf = GradientBoostingRegressor(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0)
#----MKP --10%
#clf = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000)
#clf = MLPRegressor(solver='sgd', alpha=1e-5, hidden_layer_sizes=(3,3), random_state=1, max_iter = 1000)


clf.fit(input, target)

#Store model
joblib.dump(clf, "/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

#Load model
#clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

tmp = [[]for i in range(len(target))]
for i in range (len(input)):
	tmp1 = clf.predict([input[i]])[0]
	for j in range(len(tmp1)):
		tmp[i].append(tmp1[j])

diff_total = 0
for i in range(len(target)):
	for j in range(len(target[0])):
		diff_total = diff_total + abs(tmp[i][j] - target[i][j])/target[i][j]

print float(float(diff_total)/(float(len(target) * len(target[0]))))
print (diff_total, len(target) * len(target[0]))
'''

'''
#Get average best threads
total_time = []
for i in range(current_op):
	my_file = file(time_path, "r")
	total_time.append(0)
	counter = 0
	for line in my_file:
		if counter%2 == 0:
			value = line[:-1].split("\t")[i]
			total_time[i] = total_time[i] + float(value)
		counter = counter + 1	

best_time = float("inf")
best_thread = 0
for i in range(len(total_time)):
	if best_time > total_time[i]:
		best_time = total_time[i]
		best_thread = i
print (best_time, best_thread)
'''	
import torch
from torch import nn 
import torch.nn.functional as F 
import matplotlib.pyplot as plt 
from torch.autograd import Variable

 
x = Variable(torch.Tensor(input))
y = Variable(torch.Tensor(target))

class Net(torch.nn.Module):
 
    def __init__(self, n_feature, n_hidden1, n_hidden2, n_output):
        super(Net, self).__init__()  
        self.hidden1 = nn.Sequential(nn.Linear(n_feature, n_hidden1), nn.BatchNorm1d(n_hidden1), nn.ReLU(True))
        self.hidden2 = nn.Sequential(nn.Linear(n_hidden1, n_hidden2), nn.BatchNorm1d(n_hidden2), nn.ReLU(True))
        self.predict = nn.Sequential(nn.Linear(n_hidden2, n_output))  

    def forward(self, x):
        x = F.relu(self.hidden1(x))
        x = F.relu(self.hidden2(x))     
        x = self.predict(x)            
        return x

net = Net(n_feature=3, n_hidden1=10, n_output=1)

optimizer = torch.optim.SGD(net.parameters(), lr=0.2)  
loss_func = torch.nn.MSELoss()      
 
for t in range(1000):
    prediction = net(x)    
 
    loss = loss_func(prediction, y)   
 
    optimizer.zero_grad()  
    loss.backward()        
    optimizer.step()  

print prediction.data[0]
print target[0]

diff_total = 0
for i in range(len(target)):
	diff_total = diff_total + abs(float(prediction[i]) - target[i])/target[i]

print float(float(diff_total)/(float(len(target) * len(target[0]))))
print (diff_total, len(target) * len(target[0]))

#print loss
#print target
#print prediction  


