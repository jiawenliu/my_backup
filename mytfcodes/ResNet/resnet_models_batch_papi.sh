export PAPI1=PAPI_L1_DCM
for j in {2..68..2}
        do
		export MY_PATH=/global/homes/j/jiawen/data/single_op/MKL2TF/
                export OMP_NUM_THREADS=$j
		rm -rf /global/homes/j/jiawen/tmp/resnet_model
		/global/homes/j/jiawen/models/bazel-bin/resnet/resnet_main --train_data_path=/global/homes/j/jiawen/models/cifar10/data_batch*  --train_dir=/global/homes/j/jiawen/tmp/resnet_model/train  --dataset='cifar10' --inter=1 --intra=$j
done



