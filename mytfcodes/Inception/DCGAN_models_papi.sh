PAPI[0]=PAPI_L2_TCM
PAPI[1]=PAPI_TOT_INS
PAPI[2]=PAPI_TOT_CYC
PAPI[3]=PAPI_L2_TCA

cd /global/homes/j/jiawen/DCGAN-tensorflow
for i in {0..3}
	for j in {2..34..4}
        do
		export MYPAPI=${PAPI[$i]}
		export MY_PATH=/global/homes/j/jiawen/data/single_op/DCGAN_$j/$MYPAPI
                export OMP_NUM_THREADS=$j
		python main.py --dataset mnist --input_height=28 --output_height=28 --train --inter=1 --intra=68

	for j in {40..68..4}
        do
		export MYPAPI=${PAPI[$i]}
		export MY_PATH=/global/homes/j/jiawen/data/single_op/DCGAN_$j/$MYPAPI
                export OMP_NUM_THREADS=$j
		python main.py --dataset mnist --input_height=28 --output_height=28 --train --inter=1 --intra=68
done



