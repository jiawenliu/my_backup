import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
import linecache
from sklearn import tree, svm, naive_bayes,neighbors
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression 
from sklearn.grid_search import GridSearchCV

path = "/global/homes/j/jiawen/data/single_op/Inception_68"
time_path = "/global/homes/j/jiawen/data/single_op/Inception_68/Time/Time"

op = []

#Conv2DBackpropFilter
op.append(43)
#Conv2DBackpropInput
op.append(39)
#Conv2D
op.append(48)
#MklInputConversion
op.append(138)
#Mkl2Tf
op.append(123)
#AddN
op.append(85)
#Reshape
op.append(16)
#AvgPool
op.append(20)

total_op = 0
start = 0
end = 8

for i in range(start, end):
	total_op = total_op + op[i]

#column_convert: 0,2...66
#column_mkl2tf:  1,3...67

s = [[]for i in range(total_op)]

#Add op hw data to s
for i in range(total_op):
	for files in os.listdir(path): 
		line = linecache.getline(os.path.join(path,files), i+1)
		if len(line)!=0 and len(line)!=1:
			s[i].append(line[:-1])

#Get each best thread
best_time = []
best_thread = []
total_start = 0
total_end = 0
tmp = []
for i in range (start, end):
	if i == 0:
		total_start = 0
	else:
		total_start= total_start + op[i-1]
	counter = 0	
	for j in range (total_start, total_start + op[i]):
		best_time.append(float("inf"))
		best_thread.append(2)
		for k in range (2,70,2):
			my_path = time_path + str(k)
			line = linecache.getline(my_path, j+1)
			if len(line)!=0 and len(line)!=1:
				value = line[:-1]
				if k == 2 or best_time[j] > value:
					best_time[j] = value
					best_thread[j] = k
				#Add time data to s	
				if k == 68:
					s[j].append(value)	
					s[j].append(i)
					s[j].append(counter)
					counter+=1
					for n in range(len(tmp)):
						s[j].append(tmp[n])
					tmp = []	
				else:
					tmp.append(value)	
		s[j].append(best_thread[j])			

data = [[]for i in range(len(s)-7)]
counter = 0
for d1 in range (len(s)):
	if len(s[d1])!=1:	
		for d2 in range(len(s[0])):
			data[counter].append(float(s[d1][d2]))	
		counter+=1	

print data

#Store data
df = pd.DataFrame(data)
df.to_csv("/global/homes/j/jiawen/data/single_op/Inception_data.csv", header=None, index=None)

'''
#Copy data from data[][] to input and target
input = [[]for i in range(len(data))]
target = []

#print s

for d1 in range (len(data)):
		for d2 in range(len(data[0])-1):
			if d2 ==5 or d2 == 9 or d2 ==26:
				input[counter].append(float(data[d1][d2]))		

for d1 in range(len(s)):
		target.append(data[d1][len(data[0])-1])
'''


'''
PAPI_L2_TCM 5
PAPI_L2_TCA 9
PAPI_L1_ICH 15
Time 26
Op_type 27
Op_input_index 28
'''

'''
for d1 in range(len(s)-10):
	value = s[d1][len(s[0])-1]
	if value <= 4:
		target[d1] = 4
	elif value <= 8:
		target[d1] = 8
	elif value <= 12:
		target[d1] = 12
	elif value <= 16:
		target[d1] = 16
	elif value <= 20:
		target[d1] = 20
	elif value <= 24:
		target[d1] = 24
	elif value <= 28:
		target[d1] = 28
	elif value <= 32:
		target[d1] = 32
	elif value <= 36:
		target[d1] = 36
	elif value <= 40:
		target[d1] = 40
	elif value <= 44:
		target[d1] = 44
	elif value <= 48:
		target[d1] = 48
	elif value <= 52:
		target[d1] = 52
	elif value <= 56:
		target[d1] = 56
	elif value <= 60:
		target[d1] = 60
	elif value <= 64:
		target[d1] = 64
	elif value <= 68:
		target[d1] = 68			
'''

'''
#----SVM --70%
#kernels: linear, sigmoid, rbf, poly
#clf = svm.SVC(kernel = 'linear')
#----decision_tree --90%
clf = tree.DecisionTreeClassifier()
#----naive_mul --30%
#clf = naive_bayes.MultinomialNB()
#----naive_gaussian --40%
#clf = naive_bayes.GaussianNB()
#----K_neighbor --70%
#clf = neighbors.KNeighborsClassifier()
#----bagging_knn --50%
#clf = BaggingClassifier(neighbors.KNeighborsClassifier(), max_samples=0.5,max_features=0.5)
#----bagging_tree --85%
#clf = BaggingClassifier(tree.DecisionTreeClassifier(), max_samples=0.5,max_features=0.5)
#----random_forest --90%
#clf = RandomForestClassifier(n_estimators=50)
#----adaboost --60%
#clf = AdaBoostClassifier(n_estimators=50)
#----Logistic Regression --10%
#clf = LogisticRegression(penalty='l2')
#----gradient_boost --10%
#clf = GradientBoostingClassifier(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0)
#----MKP --10%
#clf = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000)

clf.fit(input, target)

#Store model
joblib.dump(clf, "/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

#Load model
#clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

for i in range (len(target)):
	print target[i]

for i in range(5):
	print ""

tmp = []
for i in range (len(input)):
	tmp.append(clf.predict([input[i]])[0])
	print tmp[i]

#print tmp	
'''

'''
#Get average best threads
total_time = []
for i in range(current_op):
	my_file = file(time_path, "r")
	total_time.append(0)
	counter = 0
	for line in my_file:
		if counter%2 == 0:
			value = line[:-1].split("\t")[i]
			total_time[i] = total_time[i] + float(value)
		counter = counter + 1	

best_time = float("inf")
best_thread = 0
for i in range(len(total_time)):
	if best_time > total_time[i]:
		best_time = total_time[i]
		best_thread = i
print (best_time, best_thread)
'''

'''
#ANN
from pybrain.structure import FeedForwardNetwork
from pybrain.structure import LinearLayer, SigmoidLayer
from pybrain.structure import FullConnection
from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer
from scipy import diag, arange, meshgrid, where
from numpy.random import multivariate_normal

n = FeedForwardNetwork()
inLayer = LinearLayer(4)
hiddenLayer = SigmoidLayer(20)
hiddenLayer2 = SigmoidLayer(10)
outLayer = LinearLayer(1)

n.addInputModule(inLayer)
n.addModule(hiddenLayer)
n.addModule(hiddenLayer2)
n.addOutputModule(outLayer)

in_to_hidden = FullConnection(inLayer, hiddenLayer)
hidden_to_hidden = FullConnection(hiddenLayer, hiddenLayer2)
hidden_to_out = FullConnection(hiddenLayer2, outLayer)
n.addConnection(in_to_hidden)
n.addConnection(hidden_to_hidden)
n.addConnection(hidden_to_out)
n.sortModules()

alldata = ClassificationDataSet(4, nb_classes=1)

for i in range(len(input)):
	ins = (input[i][0], input[i][1], input[i][2], input[i][3])
	ous = (target[i])
	alldata.addSample(ins, ous)

tstdata, trndata = alldata.splitWithProportion( 0.20 )

# train
fnn = buildNetwork( trndata.indim, 5, trndata.outdim, outclass=SoftmaxLayer)
trainer = BackpropTrainer( fnn, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01)
for i in range(1):
	trainer.trainEpochs(20)
print fnn.activate(input[46])
'''