export MY_ITER=100
for i in {8..68..4}
do
	echo "Threads: $i"
	export OMP_NUM_THREADS=$i
	#python Conv2DBackpropFilter.py 32 8 8 2048 32 8 8 2048 1 1 2048 2048
	#python Conv2DBackpropInput.py 1 1 384 384 32 17 17 384
	python conv2d.py 32 8 8 384 1 1 384 384
done

export MY_ITER=100
for i in {8..68..4}
do
        echo "Threads: $i"
        export OMP_NUM_THREADS=$i
        python conv2d.py 32 17 17 384 1 1 384 384
done	

export MY_ITER=100
for i in {8..68..4}
do
        echo "Threads: $i"
        export OMP_NUM_THREADS=$i
        python conv2d.py 32 8 8 2048 1 1 2048 2048
done	
