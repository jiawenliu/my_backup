import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
from sklearn import tree, svm, naive_bayes,neighbors
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, RandomForestClassifier, GradientBoostingClassifier



path = []
path.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropFilter")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropInput")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2D")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/AddN")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/ReShape")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/AvgPool")

path_wo = []
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropFilter_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropInput_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropConv_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/AddN_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/ReShape_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/AvgPool_wo")

size = []
size.append(4)
size.append(5)
size.append(6)
size.append(3)
size.append(3)
size.append(2)


column = 34

total_size = 0

start = 0
end = 6

for index in range(start, end):
	total_size = total_size + size[index]

data = [[]for i in range(column*total_size)]

#Load data and train
for index in range(start, end):
	s = [[]for i in range(column*size[index])]
	best_time = []
	best_thread = []

	list_dirs = os.walk(path[index]) 
	for root, dirs, files in list_dirs:     
		dirs.sort()
		dir_index = 0
		#d includes all data in one input size of an operation
		for d in dirs: 
			threads = 2
			my_path = os.path.join(root, d)
			for i in range(column):
				#line_counter = 0
				for file in os.listdir(my_path): 
					f = open(os.path.join(my_path,file))
					iter_f = iter(f)
					counter = 0
					for line in iter_f: 
						if index == 0 or index == 1: 
							value = line[:-1].split("\t")[1]
						else:
							value = line[:-1].split("\t")[0]	
						if counter == i:
							s[i+(dir_index*column)].append(value)	
						counter=counter+1	
					#line_counter = line_counter + 1
					#if line_counter == 15:
					#	break			
				#s[i+(dir_index*column)].append(index+15)
				#s[i+(dir_index*column)].append(dir_index)	
				#s[i+(dir_index*column)].append(threads)
				threads+=2	
			dir_index+=1		
	'''	
	#Add opt_threads to each item
	for i in range(size[index]):
		best_time.append(float("inf"))
		best_thread.append(2)
		threads = 2
		for j in range(column):
			if threads == 2:
				best_time[i] = s[i * column + j][15]
			elif s[i * column + j][15] < best_time[i]:
				best_time[i] = s[i * column + j][15]
				best_thread[i] = threads
			threads = threads + 2

		for j in range(column):	
			s[i * column + j].append(best_thread[i])
	'''	

	#Add s to the total data
	current_size = 0
	for i in range(start, index):
		current_size = current_size + size[i]

	for i in range(len(s)):
		for j in range(len(s[0])):
				data[current_size * column +i].append(s[i][j]) 

my_data = [[]for i in range(total_size)]

for d1 in range (total_size):
	time = 0
	for d2 in range(len(data[0])):
		if d2 != 15:
			my_data[d1].append(float(data[d1*34 + 33][d2]))
		else:
			time = float(data[d1*34 + 33][d2])
		if d2 == 26:
			my_data[d1].append(time)


input = [[]for i in range(total_size)]
target = [[]for i in range(total_size)]

for d1 in range (total_size):
	for d2 in range(len(data[0])):
		input[d1].append(float(my_data[d1][d2]))

counter = 0
for d1 in range (total_size):
	for d2 in range(34):
		target[d1].append(float(data[d2+(d1*34)][15]))		


#Store data
df = pd.DataFrame(data)
df.to_csv("/global/homes/j/jiawen/data/single_op/ResNet_data.csv", header=None, index=None)


#Store data
#df = pd.DataFrame(data)
#df.to_csv("/global/homes/j/jiawen/data/single_op/ResNet.csv", header=None, index=None)

'''
#Load data
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/svc_linear_backup.csv")
data1 = df.values.tolist()

#Generate input and target
input_data_size = total_size
#input_data_size = len(data)
input = [[]for i in range(input_data_size)]
target = [0 for i in range (input_data_size)]

#Copy data from data[][] to input and target
for d1 in range (input_data_size):
	for d2 in range(len(data[0])-1):
		if d2 == 27 or d2 == 28 :
		#input[d1].append(float(data[d1][d2]))	
			input[d1].append(float(data[d1*34 + 33][d2]))	
'''
			
'''
PAPI_L2_TCM 5
PAPI_L2_TCA 9
PAPI_L1_ICH 16
Time 15
Op_type 27
input_size 28
'''

'''
for d1 in range(input_data_size):
	#target[d1] = data[d1][len(data[0])-1]
	target[d1] = data[d1*34+33][len(data[0])-1]

for d1 in range(input_data_size):
	value = data[d1*34+33][len(data[0])-1]
	if value <= 4:
		target[d1] = 4
	elif value <= 8:
		target[d1] = 8
	elif value <= 12:
		target[d1] = 12
	elif value <= 16:
		target[d1] = 16
	elif value <= 20:
		target[d1] = 20
	elif value <= 24:
		target[d1] = 24
	elif value <= 28:
		target[d1] = 28
	elif value <= 32:
		target[d1] = 32
	elif value <= 36:
		target[d1] = 36
	elif value <= 40:
		target[d1] = 40
	elif value <= 44:
		target[d1] = 44
	elif value <= 48:
		target[d1] = 48
	elif value <= 52:
		target[d1] = 52
	elif value <= 56:
		target[d1] = 56
	elif value <= 60:
		target[d1] = 60
	elif value <= 64:
		target[d1] = 64
	elif value <= 68:
		target[d1] = 68	
'''

'''
#Feature selection
input1 = [[]for i in range(input_data_size)]
for d1 in range (input_data_size):
	for d2 in range(len(data[0])-1):
		input1[d1].append(data[d1*34 + 33][d2])

#Choose feature based on the trees classifier
clf = ExtraTreesClassifier()
clf = clf.fit(input1, target)
#print clf.feature_importances_
model = SelectFromModel(clf, threshold='1.8*mean',prefit=True)
input_trees = model.transform(input1)
#print input_trees
'''


#Train data
#----SVM --70%
#kernels: linear, sigmoid, rbf, poly
#clf = svm.SVR(kernel = 'linear')
#----decision_tree --90%
#clf = tree.DecisionTreeClassifier()
#----naive_mul --30%
#clf = naive_bayes.MultinomialNB()
#----naive_gaussian --40%
#clf = naive_bayes.GaussianNB()
#----K_neighbor --70%
#clf = neighbors.KNeighborsClassifier()
#----bagging_knn --50%
#clf = BaggingClassifier(neighbors.KNeighborsClassifier(), max_samples=0.5,max_features=0.5)
#----bagging_tree --85%
#clf = BaggingClassifier(tree.DecisionTreeClassifier(), max_samples=0.5,max_features=0.5)
#----random_forest --90%
#clf = RandomForestClassifier(n_estimators=50)
#----adaboost --60%
#clf = AdaBoostClassifier(n_estimators=50)
#----Logistic Regression --10%
#clf = LogisticRegression(penalty='l2')
#----gradient_boost --10%
#clf = GradientBoostingClassifier(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0)
#----MKP --10%
#clf = MLPClassifier(solver='sgd', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000)

#clf.fit(input, target)

#Store model
#joblib.dump(clf, "/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

#Load model
clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")
#Predict results

#for i in range (total_size):
#	print target[i]


tmp = []
for i in range (total_size):
	tmp.append(clf.predict([input[i]])[0])
	#print tmp[i]

diff_total = 0
for i in range(len(target)):
	for j in range(len(target[0])):
		diff_total = diff_total + abs(tmp[i][j] - target[i][j])/target[i][j]

print float(float(diff_total)/float(len(target) * len(target[0])))	


#clf.predict([input[column*15+32]])
#clf = svm.SVC(kernel = 'linear')
#clf.fit(input[:-1], target[:-1])

#Model predict
#print clf.decision_function([data[34*1+0]]) 

#size.append(4) 0-3
#size.append(5) 4-8
#size.append(6) 9-14
#size.append(3) 15-17
#size.append(3) 18-20
#size.append(2) 21-22
'''
18
14
16
16
18
16
18
18
16
16
16
20
16
16
20
44
60
8
2
2
2
46
58
'''

#[18, 14, 16, 16]
#[18, 16, 18, 18, 16]
#[16, 16, 20, 16, 16, 20]
#[44, 60, 8]
#[2, 2, 2]
#[46, 58]

#clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")
#print clf.predict([input[column*0+33]]) 
