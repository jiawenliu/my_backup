import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
from sklearn import tree, svm, naive_bayes,neighbors
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, RandomForestClassifier, GradientBoostingClassifier



path = []
path.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropFilter")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropInput")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2D")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/AddN")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/ReShape")
path.append("/global/homes/j/jiawen/data/single_op/ResNet/AvgPool")

path_wo = []
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropFilter_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropInput_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/Conv2DBackpropConv_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/AddN_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/ReShape_wo")
path_wo.append("/global/homes/j/jiawen/data/single_op/ResNet/AvgPool_wo")

size = []
size.append(4)
size.append(5)
size.append(6)
size.append(3)
size.append(3)
size.append(2)


column = 34

total_size = 0

start = 0
end = 6

for index in range(start, end):
	total_size = total_size + size[index]

data = [[]for i in range(column*total_size)]

#Load data and train
for index in range(start, end):
	s = [[]for i in range(column*size[index])]
	best_time = []
	best_thread = []

	list_dirs = os.walk(path[index]) 
	for root, dirs, files in list_dirs:     
		dirs.sort()
		dir_index = 0
		#d includes all data in one input size of an operation
		for d in dirs: 
			threads = 2
			my_path = os.path.join(root, d)
			for i in range(column):
				#line_counter = 0
				for file in os.listdir(my_path): 
					f = open(os.path.join(my_path,file))
					iter_f = iter(f)
					counter = 0
					for line in iter_f: 
						if index == 0 or index == 1: 
							value = line[:-1].split("\t")[1]
						else:
							value = line[:-1].split("\t")[0]	
						if counter == i:
							s[i+(dir_index*column)].append(value)	
						counter=counter+1	
					#line_counter = line_counter + 1
					#if line_counter == 15:
					#	break			
				#s[i+(dir_index*column)].append(index+15)
				#s[i+(dir_index*column)].append(dir_index)	
				s[i+(dir_index*column)].append(threads)
				threads+=2	
			dir_index+=1		
	
	#Add opt_threads to each item
	for i in range(size[index]):
		best_time.append(float("inf"))
		best_thread.append(2)
		threads = 2
		for j in range(column):
			if threads == 2:
				best_time[i] = s[i * column + j][15]
			elif s[i * column + j][15] < best_time[i]:
				best_time[i] = s[i * column + j][15]
				best_thread[i] = threads
			threads = threads + 2

		for j in range(column):	
			s[i * column + j].append(best_thread[i])
	

	#Add s to the total data
	current_size = 0
	for i in range(start, index):
		current_size = current_size + size[i]

	for i in range(len(s)):
		for j in range(len(s[0])):
				data[current_size * column +i].append(s[i][j]) 
	#print best_time
	print best_thread

print data

#Store data
df = pd.DataFrame(data)
df.to_csv("/global/homes/j/jiawen/data/single_op/test.csv", header=None, index=None)


