import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
import linecache
from sklearn import tree, svm, naive_bayes,neighbors
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression 
from sklearn.grid_search import GridSearchCV

path = "/global/homes/j/jiawen/data/single_op/ResNet/MKL2TF"
time_path = "/global/homes/j/jiawen/data/single_op/ResNet/MKL2TF/Time"

column_convert = 49
column_mkl2tf = 27

#column_convert: 0,2...66
#column_mkl2tf:  1,3...67

s = [[]for i in range(column_convert + column_mkl2tf)]

#Add op data to s
for i in range(column_convert):
	for files in os.listdir(path): 
		line = linecache.getline(os.path.join(path,files), 67)
		s[i].append(line[:-1].split("\t")[i])

#Get each best thread
best_time = []
best_thread = []
for i in range(column_convert):
	my_file = file(time_path, "r")
	threads = 2
	best_time.append(float("inf"))
	best_thread.append(2)
	counter = 0
	for line in my_file:
		if counter%2 == 0:
			value = line[:-1].split("\t")[i]
			if threads == 2 or best_time[i] > value:
				best_time[i] = value
				best_thread[i] = threads 
			threads = threads + 2	
		counter = counter + 1	
counter = 0	
for i in range (column_convert):
	s[i].append(21)
	s[i].append(i)
	s[i].append(best_thread[i])

#Add op data to s
for i in range(column_mkl2tf):
	for files in os.listdir(path): 
		line = linecache.getline(os.path.join(path,files), 68)
		s[column_convert+i].append(line[:-1].split("\t")[i])

#Get each best thread
best_time = []
best_thread = []
for i in range(column_mkl2tf):
	my_file = file(time_path, "r")
	threads = 2
	best_time.append(float("inf"))
	best_thread.append(2)
	counter = 0
	for line in my_file:
		if counter%2 == 1:
			value = line[:-1].split("\t")[i]
			if threads == 2 or best_time[i] > value:
				best_time[i] = value
				best_thread[i] = threads 
			threads = threads + 2	
		counter = counter + 1	
counter = 0		
for i in range (column_mkl2tf):
	s[column_convert+i].append(22)
	s[column_convert+i].append(i)
	s[column_convert+i].append(best_thread[i])

print s

#Store data
df = pd.DataFrame(s)
df.to_csv("/global/homes/j/jiawen/data/single_op/ResNet_data.csv", mode='a', header=None, index=None)


'''
#Copy data from data[][] to input and target
input = [[]for i in range(len(s)-10)]
target = [0 for i in range (len(s)-10)]
test = [[]for i in range (10)]

counter = 0
for d1 in range (len(s)):
	for d2 in range(len(s[0])-1):
		if d1 < len(s)-10:
			#if d2 == 5 or d2 == 9:
				input[d1].append(float(s[d1][d2]))	
		else:
			#if d2 == 5 or d2 == 9:
				test[counter].append(float(s[d1][d2]))	 
	if	d1 >= len(s)-10:		
		counter = counter + 1						

for d1 in range(len(s)-10):
	target[d1] = s[d1][len(s[0])-1]


PAPI_L2_TCM 5
PAPI_L2_TCA 9
PAPI_L1_ICH 16
Time 15
Op_type 27
'''
'''
for d1 in range(len(s)-10):
	value = s[d1][len(s[0])-1]
	if value <= 4:
		target[d1] = 4
	elif value <= 8:
		target[d1] = 8
	elif value <= 12:
		target[d1] = 12
	elif value <= 16:
		target[d1] = 16
	elif value <= 20:
		target[d1] = 20
	elif value <= 24:
		target[d1] = 24
	elif value <= 28:
		target[d1] = 28
	elif value <= 32:
		target[d1] = 32
	elif value <= 36:
		target[d1] = 36
	elif value <= 40:
		target[d1] = 40
	elif value <= 44:
		target[d1] = 44
	elif value <= 48:
		target[d1] = 48
	elif value <= 52:
		target[d1] = 52
	elif value <= 56:
		target[d1] = 56
	elif value <= 60:
		target[d1] = 60
	elif value <= 64:
		target[d1] = 64
	elif value <= 68:
		target[d1] = 68			
'''
'''
#----SVM --70%
#kernels: linear, sigmoid, rbf, poly
#clf = svm.SVC(kernel = 'linear')
#----decision_tree --90%
clf = tree.DecisionTreeClassifier()
#----naive_mul --30%
#clf = naive_bayes.MultinomialNB()
#----naive_gaussian --40%
#clf = naive_bayes.GaussianNB()
#----K_neighbor --70%
#clf = neighbors.KNeighborsClassifier()
#----bagging_knn --50%
#clf = BaggingClassifier(neighbors.KNeighborsClassifier(), max_samples=0.5,max_features=0.5)
#----bagging_tree --85%
#clf = BaggingClassifier(tree.DecisionTreeClassifier(), max_samples=0.5,max_features=0.5)
#----random_forest --90%
#clf = RandomForestClassifier(n_estimators=50)
#----adaboost --60%
#clf = AdaBoostClassifier(n_estimators=50)
#----Logistic Regression --10%
#clf = LogisticRegression(penalty='l2')
#----gradient_boost --10%
#clf = GradientBoostingClassifier(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0)
#----MKP --10%
#clf = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000)

clf.fit(input, target)

#Store model
joblib.dump(clf, "/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

#Load model
#clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")

for i in range (len(s)-10):
	print target[i]
print ""

tmp = []
for i in range (len(s)-10):
	tmp.append(clf.predict([input[i]])[0])
	print tmp[i]

tmp = []
for i in range (10):
	tmp.append(clf.predict([test[i]])[0])
	print tmp[i]

#print tmp	
'''

'''
#Get average best threads
total_time = []
for i in range(current_op):
	my_file = file(time_path, "r")
	total_time.append(0)
	counter = 0
	for line in my_file:
		if counter%2 == 0:
			value = line[:-1].split("\t")[i]
			total_time[i] = total_time[i] + float(value)
		counter = counter + 1	

best_time = float("inf")
best_thread = 0
for i in range(len(total_time)):
	if best_time > total_time[i]:
		best_time = total_time[i]
		best_thread = i
print (best_time, best_thread)
'''

'''
#ANN
from pybrain.structure import FeedForwardNetwork
from pybrain.structure import LinearLayer, SigmoidLayer
from pybrain.structure import FullConnection
from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer
from scipy import diag, arange, meshgrid, where
from numpy.random import multivariate_normal

n = FeedForwardNetwork()
inLayer = LinearLayer(4)
hiddenLayer = SigmoidLayer(20)
hiddenLayer2 = SigmoidLayer(10)
outLayer = LinearLayer(1)

n.addInputModule(inLayer)
n.addModule(hiddenLayer)
n.addModule(hiddenLayer2)
n.addOutputModule(outLayer)

in_to_hidden = FullConnection(inLayer, hiddenLayer)
hidden_to_hidden = FullConnection(hiddenLayer, hiddenLayer2)
hidden_to_out = FullConnection(hiddenLayer2, outLayer)
n.addConnection(in_to_hidden)
n.addConnection(hidden_to_hidden)
n.addConnection(hidden_to_out)
n.sortModules()

alldata = ClassificationDataSet(4, nb_classes=1)

for i in range(len(input)):
	ins = (input[i][0], input[i][1], input[i][2], input[i][3])
	ous = (target[i])
	alldata.addSample(ins, ous)

tstdata, trndata = alldata.splitWithProportion( 0.20 )

# train
fnn = buildNetwork( trndata.indim, 5, trndata.outdim, outclass=SoftmaxLayer)
trainer = BackpropTrainer( fnn, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01)
for i in range(1):
	trainer.trainEpochs(20)
print fnn.activate(input[46])
'''