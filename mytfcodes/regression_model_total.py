import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
import linecache
from sklearn import tree, svm, naive_bayes,neighbors
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression 
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import mean_absolute_error, r2_score, make_scorer, mean_squared_error
from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import train_test_split


#Load Inception data
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/Inception_data.csv")
data_Inception = df.values.tolist()

#Load DCGAN data
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/DCGAN_data.csv")
data_DCGAN = df.values.tolist()

#Load all data
#total_len = len(data_Inception) + len(data_DCGAN) +2
total_len = len(data_Inception)
data = [[]for i in range(total_len)]

for i in range(len(data_Inception)):
	for j in range (len(data_Inception[0])):
		data[i].append(data_Inception[i][j])

#for i in range(len(data_DCGAN)):
#	for j in range (len(data_DCGAN[0])):
#		data[len(data_Inception)+i].append(data_DCGAN[i][j])

input = [[]for i in range(total_len)]
target = []

#Copy test data to training_input and training_target




counter = 0
for d1 in range (total_len):
	#if d1 % 4 == 0 and d1 != 0:
	#	counter = counter + 1
	for d2 in range(len(data[0])-34):
		#Raw PC
		#if d2<2:
		#PC
		if d2 == 5 or d2 == 9 or d2 ==24 or d2 == 3:
		#if d2 ==24 or d2 == 3 or d2 == 15:
		#OC
		#if d2 == 27 or d2 == 28:
		#PC+OC
		#if d2 == 5 or d2 == 9 or d2 == 15 or d2 ==26 or d2 == 27 or d2 == 28:
		#if d2 == 1 or d2 == 2 or d2 == 3 or d2 ==4 or d2 == 9 or d2 == 16 or d2 == 17 or d2 == 23:
			#input[d1].append(float(data[d1][d2]))
			input[d1].append(float(data[d1][d2]))

counter = 0
for d1 in range(total_len):
	#if d1 % 4 == 0:
	#	counter = counter + 1
		for d2 in range(2,3):
		#target[counter].append(float(data[d1][len(data[d1])-d2]))
			target.append(float(data[d1][len(data[d1])-d2]))

op = []
#Conv2DBackpropFilter
op.append(43)
#Conv2DBackpropInput
op.append(39)
#Conv2D
op.append(48)
#MklInputConversion
op.append(138)
#Mkl2Tf
op.append(123)
#AddN
op.append(85)
#Reshape
op.append(16)
#AvgPool
op.append(20)

total_op = 0

for i in range(0, 8):
	total_op = total_op + op[i]

s = [[]for i in range(total_op)]

path = []
path.append("/global/homes/j/jiawen/data/single_op/Inception_10")
path.append("/global/homes/j/jiawen/data/single_op/Inception_14")
path.append("/global/homes/j/jiawen/data/single_op/Inception_18")
path.append("/global/homes/j/jiawen/data/single_op/Inception_26")
path.append("/global/homes/j/jiawen/data/single_op/Inception_30")
path.append("/global/homes/j/jiawen/data/single_op/Inception_40")
path.append("/global/homes/j/jiawen/data/single_op/Inception_44")
path.append("/global/homes/j/jiawen/data/single_op/Inception_48")
path.append("/global/homes/j/jiawen/data/single_op/Inception_52")
path.append("/global/homes/j/jiawen/data/single_op/Inception_56")
path.append("/global/homes/j/jiawen/data/single_op/Inception_60")
path.append("/global/homes/j/jiawen/data/single_op/Inception_64")

for k in range(1,10):
	counter = 0
	for i in range(total_op):
		for files in os.listdir(path[k]): 
			line = linecache.getline(os.path.join(path[k],files), i+1)
			if len(line)!=0 and len(line)!=1:
				s[i].append(line[:-1])			

	my_s = [[]for i in range(len(s)-8)]
	counter = 0
	for d1 in range (len(s)-1):
		if len(s[d1])!=0:
			for d2 in range(len(s[d1])):
				my_s[counter].append(float(s[d1][d2]))	
			counter+=1	
	
	for d1 in range (len(my_s)):
		for d2 in range (len(my_s[0])):
			input[d1].append(my_s[d1][d2])
	



input_train, input_test, target_train, target_test = train_test_split(input,target,test_size=0.2, random_state=0)


#print target	
#print data

'''
Index:
PAPI_L2_TCM 5
PAPI_L2_TCA 9
PAPI_L1_ICH 15
PAPI_TOT_CYC 3
PAPI_TOT_INC 24
Time 26
Op_type 27
Op_input_index 28
Best thread 29
'''
'''
PAPI_L2_TCM 0
PAPI_L2_TCA 1
Time 2
Op_type 3
Op_input_index 4
'''

#Choose feature based on the trees classifier
'''
clf = ExtraTreesRegressor()
clf = clf.fit(input_train, target_train)
print clf.feature_importances_
model = SelectFromModel(clf, threshold='4*mean',prefit=True)
input_trees = model.transform(input_train)
'''



clf = []
from sklearn.tree import DecisionTreeRegressor
from sklearn.multioutput import MultiOutputRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import BaggingClassifier, BaggingRegressor, AdaBoostClassifier, AdaBoostRegressor, RandomForestClassifier, GradientBoostingRegressor, GradientBoostingClassifier

clf.append(GradientBoostingRegressor(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0))
clf.append(KNeighborsRegressor())
clf.append(linear_model.TheilSenRegressor())
clf.append(linear_model.LinearRegression())
clf.append(linear_model.PassiveAggressiveRegressor())
clf.append(DecisionTreeRegressor())
clf.append(RandomForestRegressor(n_estimators=50))
clf.append(linear_model.ARDRegression())

#57.96%

#Bad
#clf.append(MultiOutputRegressor(AdaBoostRegressor(n_estimators=50)))
#Bad
#clf.append(MultiOutputRegressor(linear_model.ARDRegression()))

#Bad
#clf.append(MultiOutputRegressor(linear_model.PassiveAggressiveRegressor()))

#Bad
#clf.append(MultiOutputRegressor(linear_model.TheilSenRegressor()))

#Bad


#46.62%
#clf.append(MultiOutputRegressor(KNeighborsRegressor()))


#Bad
#clf.append(MultiOutputRegressor(RandomForestRegressor(n_estimators=50)))

#23,14%
#clf.append(MultiOutputRegressor(GradientBoostingRegressor(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0)))


'''
#----SVM 0-3
#kernels: linear, poly, sigmoid, rbf
clf.append(svm.SVC(kernel = 'linear'))
clf.append(svm.SVC(kernel = 'poly'))
clf.append(svm.SVC(kernel = 'sigmoid'))
clf.append(svm.SVC(kernel = 'rbf'))
#----decision tree 4
clf.append(tree.DecisionTreeClassifier())
#----naive_mul 5
clf.append(naive_bayes.MultinomialNB())
#----naive_gaussian 6
clf.append(naive_bayes.GaussianNB())
#----K_neighbor 7
clf.append(neighbors.KNeighborsClassifier())
#----bagging_knn 8
clf.append(BaggingClassifier(neighbors.KNeighborsClassifier(), max_samples=0.5,max_features=0.5))
#----bagging_tree 9
clf.append(BaggingClassifier(tree.DecisionTreeClassifier(), max_samples=0.5,max_features=0.5))
#----Random forest 10
clf.append(RandomForestClassifier(n_estimators=50))
#----adaboost 11
clf.append(AdaBoostClassifier(n_estimators=50))
#----Logistic Regression 12
clf.append(LogisticRegression(penalty='l2'))
#----gradient_boost 13
clf.append(GradientBoostingClassifier(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0))
#----MKP 14-16
#solver: sgd, adam, lbfgs
clf.append(MLPClassifier(solver='sgd', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000))
clf.append(MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000))
clf.append(MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000))
'''

for k in range(len(clf)):
	clf[k].fit(input_train, target_train)

	#tmp = [[]for i in range(len(target_test))]
	tmp = []
	for j in range (len(input_test)):
		tmp1 = clf[k].predict([input_test[j]])[0]
		tmp.append(float(tmp1))
		#for i in range(len(tmp1)):
		#	tmp[j].append(float(tmp1[i]))
	#print target		
	#print tmp
 

	diff_total = 0
	for i in range(len(target_test)):
		#for j in range(len(target_test[0])):
		#	diff_total = diff_total + abs(tmp[i][j] - target_test[i][j])/target_test[i][j]
		diff_total = diff_total + abs(tmp[i] - target_test[i])/target_test[i]

	print float(diff_total)/(float(len(target_test)))
	#print (diff_total, len(target_test) * len(target_test[0]))
	print r2_score(target_test,tmp)
	print ""
	#print mean_absolute_error(target,tmp)

	#mean_absolute_error, r2_score, make_scorer, mean_squared_error
#	my_scorer = make_scorer(r2_score)
#	print(cross_val_score(clf[k], input, target, cv=4, scoring=my_scorer))
	#print tmp


