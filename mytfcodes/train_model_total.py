import os
import pandas as pd 
from numpy.testing import assert_almost_equal
from sklearn import datasets
from sklearn import svm
from sklearn.externals import joblib
import lightgbm as lgb
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
import linecache
from sklearn import tree, svm, naive_bayes,neighbors
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression 
from sklearn.grid_search import GridSearchCV

#Load Inception data
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/Inception_data.csv")
data_Inception = df.values.tolist()

#Load DCGAN data
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/DCGAN_data.csv")
data_DCGAN = df.values.tolist()

#Load ResNet data
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/ResNet_data.csv")
data_ResNet = df.values.tolist()

#Load Test data
df = pd.read_csv("/global/homes/j/jiawen/data/single_op/Inception_data_test_input.csv")
data_Test = df.values.tolist()

#Load all data
total_len = len(data_Inception) + len(data_DCGAN) + len(data_ResNet)
data = [[]for i in range(total_len)]

for i in range(len(data_Inception)):
	for j in range (len(data_Inception[0])):
		data[i].append(data_Inception[i][j])

for i in range(len(data_DCGAN)):
	for j in range (len(data_DCGAN[0])):
		data[len(data_Inception)+i].append(data_DCGAN[i][j])

for i in range(len(data_ResNet)):
	for j in range (len(data_ResNet[0])):
		data[len(data_Inception) + len(data_DCGAN) + i].append(data_ResNet[i][j])		

input = [[]for i in range(total_len)]
target = []

test_input = [[]for i in range(len(data_Test))]
test_target = []

print data

print data_Test

#Copy test data to training_input and training_target
for d1 in range (total_len):
	for d2 in range(len(data[0])-1):
		if d2 == 5 or d2 ==9 or d2 == 26:
			if d2 == 	5:
				input[d1].append(float(data[d1][d2])/10000000)
			elif d2 == 	9:
				input[d1].append(float(data[d1][d2])/10000000)
			elif d2 == 26:	
				input[d1].append(float(data[d1][d2]))
			else:
				input[d1].append(float(data[d1][d2]))

for d1 in range(total_len):
	target.append(data[d1][len(data[d1])-1])

#Copy test data to test_input and test_target
for d1 in range (len(data_Test)):
	for d2 in range(len(data_Test[0])):
		if d2 == 0 or  d2 == 1 or d2 == 2:
			if d2 == 0:
				test_input[d1].append(float(data_Test[d1][d2])/10000000)	
			elif d2 == 1:
				test_input[d1].append(float(data_Test[d1][d2])/10000000)	
			elif d2 == 2:
				test_input[d1].append(float(data_Test[d1][d2]))	
			else:
				test_input[d1].append(float(data_Test[d1][d2]))	
	

for d1 in range(len(data_Inception)):
	test_target.append(data_Inception[d1][len(data_Inception[d1])-1])

'''
Index:
PAPI_L2_TCM 5
PAPI_L2_TCA 9
PAPI_L1_ICH 15
Time 26
Op_type 27
Op_input_index 28
Best thread 29
'''
'''
PAPI_L2_TCM 0
PAPI_L2_TCA 1
Time 2
Op_type 3
Op_input_index 4
'''

'''
#Choose feature based on the trees classifier
clf = ExtraTreesClassifier()
clf = clf.fit(input, target)
print clf.feature_importances_
model = SelectFromModel(clf, threshold='1.8*mean',prefit=True)
input_trees = model.transform(input)
print input_trees.shape
'''


clf = []
#----SVM 0-3
#kernels: linear, poly, sigmoid, rbf
clf.append(svm.SVC(kernel = 'linear'))
clf.append(svm.SVC(kernel = 'poly'))
clf.append(svm.SVC(kernel = 'sigmoid'))
clf.append(svm.SVC(kernel = 'rbf'))
#----decision tree 4
clf.append(tree.DecisionTreeClassifier())
#----naive_mul 5
clf.append(naive_bayes.MultinomialNB())
#----naive_gaussian 6
clf.append(naive_bayes.GaussianNB())
#----K_neighbor 7
clf.append(neighbors.KNeighborsClassifier())
#----bagging_knn 8
clf.append(BaggingClassifier(neighbors.KNeighborsClassifier(), max_samples=0.5,max_features=0.5))
#----bagging_tree 9
clf.append(BaggingClassifier(tree.DecisionTreeClassifier(), max_samples=0.5,max_features=0.5))
#----Random forest 10
clf.append(RandomForestClassifier(n_estimators=50))
#----adaboost 11
clf.append(AdaBoostClassifier(n_estimators=50))
#----Logistic Regression 12
clf.append(LogisticRegression(penalty='l2'))
#----gradient_boost 13
clf.append(GradientBoostingClassifier(n_estimators=50, learning_rate=1.0,max_depth=1, random_state=0))
#----MKP 14-16
#solver: sgd, adam, lbfgs
clf.append(MLPClassifier(solver='sgd', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000))
clf.append(MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000))
clf.append(MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, max_iter = 1000))

for i in range(len(clf)):
	if i >1:
		clf[i].fit(input, target)
		#Store model
		#joblib.dump(clf, "/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")
		#Load model
		#clf = joblib.load("/global/homes/j/jiawen/mytfcodes/svc_linear_backup.pkl")
		result = []
		for j in range (len(test_input)):
			result.append(clf[i].predict([test_input[j]])[0])
		'''
		correct = 0
		for j in range(len(test_target)):
			if abs(result[j] - test_target[j]) == 0:
				correct+=1		
		print (correct, len(test_input))
		'''
		#print (i, result)

