#!/bin/bash

#module load tensorflow/intel-head
#module load tensorflow/intel-1.6.0-py27
module load tensorflow/intel-1.9.0-py27

python ./tf_cnn_benchmarks.py  --mkl=True --forward_only=False --num_batches=100 --kmp_blocktime=1 --num_warmup_batches=50 --num_inter_threads=2 --distortions=False --optimizer=sgd --batch_size=256 --num_intra_threads=136 --data_format=NCHW --num_omp_threads=136 --model=alexnet 2>&1 | tee out_alexnet_1

python ./tf_cnn_benchmarks.py  --mkl=True --forward_only=False --num_batches=100 --kmp_blocktime=1 --num_warmup_batches=50 --num_inter_threads=4 --distortions=False --optimizer=sgd --batch_size=96 --num_intra_threads=66 --data_format=NCHW --num_omp_threads=66 --model=googlenet  2>&1 | tee out_googlenet_1

python ./tf_cnn_benchmarks.py  --mkl=True --forward_only=False --num_batches=100 --kmp_blocktime=1 --num_warmup_batches=50 --num_inter_threads=2 --distortions=False --optimizer=sgd --batch_size=128 --num_intra_threads=68 --data_format=NCHW --num_omp_threads=68 --model=vgg11 2>&1 | tee out_vgg11_1

python ./tf_cnn_benchmarks.py  --mkl=True --forward_only=False --num_batches=100 --kmp_blocktime=0 --num_warmup_batches=50 --num_inter_threads=3 --distortions=False --optimizer=sgd --batch_size=128 --num_intra_threads=66 --data_format=NCHW --num_omp_threads=66 --model=resnet50 2>&1 | tee out_resnet50_1

python ./tf_cnn_benchmarks.py  --mkl=True --forward_only=False --num_batches=100 --kmp_blocktime=0 --num_warmup_batches=50 --num_inter_threads=4 --distortions=False --optimizer=sgd --batch_size=64 --num_intra_threads=66 --data_format=NCHW --num_omp_threads=66 --model=inception3 2>&1 | tee out_inception3_1
