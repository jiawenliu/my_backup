#!/bin/bash

versions="intel-head intel-1.6.0-py27 intel-1.9.0-py27"
models="alexnet googlenet vgg11 inception3 resnet50"

echo "Models $versions"

for m in $models; do
    #echo $m
    rates=""
    for v in $versions; do
        rate=$(grep "total images/sec" results_$v/out_${m}_1 | awk '{print $3}')
        rates="$rates $rate"
        #echo "  $v $rate"
    done
    echo "$m $rates"
done
