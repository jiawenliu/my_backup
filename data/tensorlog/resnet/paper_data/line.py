import sys
import argparse
import json
from operator import itemgetter

my = open("hyper_threading_timeline.txt", "r")

orig_stdout = sys.stdout
f = open('out', 'w')
sys.stdout = f

counter = 0
for line in my:
	if counter <8000:
		counter = counter+1
		if counter % 4 == 1 and int(line[0]) == 1:
			print int(line[0])+1
		else:
			print line[0]
    
my.close

sys.stdout = orig_stdout
f.close()
