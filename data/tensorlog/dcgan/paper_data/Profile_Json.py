import sys
import argparse
import json
from operator import itemgetter

parser = argparse.ArgumentParser(description='This program aggregates information regarding cpu usage percentages of a tensorflow graph. To be used with TensorFlow\'s Chrome trace formated json.')
parser.add_argument('filename')
args = parser.parse_args()
raw_source = open(args.filename)
decoded_json = json.load(raw_source)

# eliminate 'M' type entries as they don't conform to structure
while decoded_json['traceEvents'][0]['ph'] == "M":
	del decoded_json['traceEvents'][0]
# sort in ascending order of operation start time
source = sorted(decoded_json['traceEvents'], key=itemgetter('ts'))
operation_list = []
operation_count = {}
operation_time = {}
total_time_working = 0
finish_time = 0
start_time = 0

for i in source:
	if i['ph'] == 'X':
		if start_time == 0:
			start_time = i['ts']
		if i['name'] not in operation_list:
			operation_list.append(i['name'])
			operation_count[i['name']] = 0
			operation_time[i['name']] = 0
		operation_count[i['name']] += 1
		operation_time[i['name']] += i['dur']
		total_time_working += i['dur']
		if finish_time < i['ts']+i['dur']:
			finish_time = i['ts']+i['dur']
sorted_profile = sorted(operation_time.items(), key=itemgetter(1), reverse=True)

orig_stdout = sys.stdout
f = open('out.txt', 'w')
sys.stdout = f

for j in sorted_profile:
	#print str(operation_count[j[0]]) + '\t' + str(j[0]) + '\t' + str(operation_time[j[0]]*0.001)
	print str(j[0]) + '\t' + str(operation_time[j[0]]*0.001)
print 'Overall, ' + str(total_time_working*0.001) + 'ms spent working out of a ' + str((finish_time-start_time)*0.001) + 'ms runtime.'

sys.stdout = orig_stdout
f.close()
