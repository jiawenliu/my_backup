import sys
import argparse
import json
from operator import itemgetter

my = open("operation_count", "r")

orig_stdout = sys.stdout
f = open('out', 'w')
sys.stdout = f

counter = 0
for line in my:
	counter = counter+1
	if counter == 10:
		counter = 0
		print line[0]
    
my.close

sys.stdout = orig_stdout
f.close()